package model;

public class Card {
	private String activity ;
	private double Money ;
	
	public Card (){
		this.Money = 0;
		
	}
	
	public void deposite (int a){
		this.activity = "Deposite" ;
		this.Money += a ;
		
	}
	
	public void withdraw (int b){
		this.activity = "Withdraw" ;
		this.Money -= b ;
		
	}
	
	public double chk (){
		this.activity = "CheckMoney" ;
		return this.Money ;
		
	}
	public String toString(){
		return activity + " :  " + Money + " Bath" ; 
	}
	
}
