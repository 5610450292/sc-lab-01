package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

import model.Card;

import gui.SoftwareFrame;

public class SoftwareTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		/*
		Student stu1 = new Student(1);
		Student stu2 = new Student(2);
		stu1.setName("SomChai");
		stu2.setName("Panupong Wongsiri");
		frame.setResult(stu1.toString());
		frame.extendResult("\t" + stu2.toString());
		 */
		Card card = new Card() ;
		card.deposite(100) ;
		frame.setResult(card.toString());
		card.withdraw(100) ;
		frame.extendResult(card.toString());
		card.deposite(100) ;
		frame.extendResult(card.toString());
		card.chk() ;
		frame.extendResult(card.toString());
	}


	ActionListener list;
	SoftwareFrame frame;
}
